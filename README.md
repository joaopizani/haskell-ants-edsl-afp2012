Haskell Ant EDSL - AFP2012 - Utrecht University
===============================================

What is this?
-------------

A Haskell-based embedded domain-specific language for writing ant strategies. Final programming
assignment of the master's course "Advanced Functional Programming" of Utrecht University,
edition 2012/2013.

I developed this project together with my friends and colleagues
[Liewe Thomas van Binsbergen](http://www.linkedin.com/pub/liewe-thomas-van-binsbergen/3a/587/659)
and João Alpuim.


Where to read a longer, nicer description of what this is about?
----------------------------------------------------------------

In the blog post about this project:

[English](http://joaopizani.hopto.org/en/2013/03/02/haskell-ants-edsl)
[Portuguese](http://joaopizani.hopto.org/2013/03/02/haskell-ants-edsl)

